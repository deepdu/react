import {createStore} from 'redux';
import methods from "./method"
import data from "./data"

var ActionFnObj=methods
function reducer(state=data,action){
	// console.log(action.type.indexOf('@@redux/'))
	if(action.type.indexOf('@@redux/')!==0){
		state=ActionFnObj[action.type](state,action)
	}
	return {...state}
}
	const store=createStore(reducer)

export default store