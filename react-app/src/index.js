import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
//导入数据和函数
import store from "./store/store.js";
//路由
import {HashRouter as Router,Route} from 'react-router-dom'
//页面
import App from './view/App.js'
import Suiji from './view/Suiji.js'
import Result from './view/Result.js'
import Guanqia from './view/Guanqia.js'
import GuanqiaDati from './view/Guanqia-dati.js'
import "./assets/css/style.css"

ReactDOM.render(
<Provider store={store}>
<Router>
	<Route path="/" exact component={App}></Route>
	<Route path="/suiji" exact component={Suiji}></Route>
	<Route path="/result" exact component={Result}></Route>
	<Route path="/guanqia" exact component={Guanqia}></Route>
	<Route path="/guanqia-dati" exact component={GuanqiaDati}></Route>
</Router>
</Provider>,

document.querySelector("#root"));