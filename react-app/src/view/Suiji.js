import React from "react";
//redux数据转props模块
import {connect} from 'react-redux';
//导入异步请求
import {getList} from "../store/asyncMethods.js"
import "../assets/css/dati.css"
import loading from "../assets/img/loading.gif"

class Dati extends React.Component{
	constructor(props){
		super(props)
		this.state={
			corrent:0,//初始题目索引
			liClass:["",'','',''],//题目选项样式数组
			isChoose:false,//选中状态
			score:0//分数
		}
	}
	render(){
		let timuList=this.props.timuList//题目数组
		let corrent=this.state.corrent;
		
		if(timuList.length>0){
			let options=JSON.parse(timuList[corrent].options)
			return(
				<div>
					<h2>{corrent+1}、{timuList[corrent].quiz}</h2>
					<div className="itemBox">
						<ul>
							{options.map((item,index)=>{
								return <li key={index} onClick={()=>{this.answerEvent(index)}} className={this.state.liClass[index]}>{index+1}:{item}</li>
							})}
						</ul>
					</div>
				</div>
			)
		}else{
			return(
				<div>
					<img alt="loading" src={loading} className="loading"/>
				</div>
			)
		}
		
	}
	//渲染之前
	 componentWillMount(){
		this.props.getTimu();
	}
	answerEvent=(index)=>{
		if(this.state.isChoose){
			return
		}
		let select=index+1;
		let answer=Number(this.props.timuList[this.state.corrent].answer);
		let liClass=this.state.liClass;
		//答题正确
		if(select===answer){
			liClass[index]="right"
			this.setState({
				liClass:liClass,
				isChoose:true,
				score:this.state.score+10
			})
		}else{
			liClass[index]="error"
			liClass[answer-1]='right'
			this.setState({
				liClass:liClass,
				isChoose:true
			})
		}
		setTimeout(()=>{
			let corrent=this.state.corrent
			if(corrent===9){//答了10题后自动跳转页面
				let score=this.state.score
				// this.props.history.push("/result",state:{score:score})
				this.props.history.push({
				  pathname: '/result',
				  state:{score:score}
				})
			}else{
				this.setState({
					liClass:['','','',''],
					corrent:++corrent,
					isChoose:false
				})
			}
		},2000)
	}
}

//将state映射到props函数
function mapStateToProps(state){
	return{
		...state
	}
}

//将修改state数据的方法，映射到props，默认会传入store里的dispatch方法
function mapDispatchToProps(dispatch){
	return {
		getTimu:async ()=>{
			let list=await getList();
			dispatch({
				type:"setTimu",
				content:list.data
			})
		}
	}
}	
//将上面的这两个方法，将数据仓库的state和修改state的方法映射到组件上，形成新的组件
const App=connect(
	mapStateToProps,
	mapDispatchToProps
)(Dati)
export default App