import React from "react";
import {connect} from 'react-redux';
// import guanqia from '../assets/img/guanqia.png'
import xing from '../assets/img/xing.png'
import "../assets/css/guanqia.css"
class Counter extends React.Component{
	render(){
		let data=JSON.parse(localStorage.getItem('data'));
		return(
			<div>
				<ul className="Guanqia-ul">
				{data.map((item,index)=>{
					return (
						<li className="Guanqia-li" key={index} onClick={()=>{this.goDati(item.corrent)}}>{item.corrent}
							<div>
								{	
									item.xing.map((item,index)=>{
										return (
											<img src={xing} alt="xing" key={index}/>
										)
									})
								}
							</div>
						</li>
					)
				})}
					
				</ul>
			</div>
		)
	}
	
	goDati=(corrent)=>{
		console.log(corrent)
		// this.props.history.push('/guanqia-dati',corrent)
		this.props.history.push({
		  pathname: '/guanqia-dati',
		  state:{page:corrent}
		})
	}
	
	componentWillMount(){
		if(!window.localStorage){
			alert('浏览器不支持localStorage');
			return false;
		}else{
			if(localStorage.getItem('data')==null){
				let arr=[
					{corrent:1,xing:[]},
				]
				window.localStorage.setItem('data',JSON.stringify(arr));
			}
		}
	}
	
}

//将state映射到props函数
function mapStateToProps(state){
	return{
		...state
	}
}

//将修改state数据的方法，映射到props，默认会传入store里的dispatch方法
function mapDispatchToProps(dispatch){
	return {
		
		
	}
}	
//将上面的这两个方法，将数据仓库的state和修改state的方法映射到组件上，形成新的组件
const App=connect(
	mapStateToProps,
	mapDispatchToProps
)(Counter)
export default App