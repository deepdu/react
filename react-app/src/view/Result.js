import React from "react";
import {connect} from 'react-redux';
import {Button} from 'antd-mobile';
import "../assets/css/result.css"
class Counter extends React.Component{
	render(){
		console.log(this.props)
		if(this.props.location.state.page){
			var tipsErr = this.props.location.state.score<=60?'抱歉,你未解锁新的关卡':'';
			var tipsRight = this.props.location.state.score>60?'你有一个新的关卡解锁':'';
		}
		return(
			<div className="bg">
				<h2 className="defen">恭喜你得分: {this.props.location.state.score}</h2>
				{this.props.location.state.score<=60?<h3 className="tipsErr">{tipsErr}</h3>:''}
				{this.props.location.state.score>60?<h3 className="tipsRight">{tipsRight}</h3>:''}
				
				
				<Button onClick={this.goHome} type="primary">{this.props.location.state.page?'回到关卡页':'回到首页'}</Button>
			</div>
		)
	}
	goHome=()=>{
		if(this.props.location.state.page){
			this.props.history.push('/guanqia');
		}else{
			this.props.history.push('/');
		}
	}
	componentWillMount(){
		if(this.props.location.state.page){
			
			console.log(this.props)
			let score=this.props.location.state.score;
			//数组索引
			let pageIndex=this.props.location.state.page-1;
			let data=JSON.parse(localStorage.getItem('data'));
			switch(true){
				case Number(score)>=100:
					//验证过去的分数是否大于当前获取的分数
					if(data[pageIndex].xing.length<3){
						data[pageIndex].xing=[1,2,3];
					}
					//判断是否是玩过去的关卡，如果是将不添加数组长度
					if(data.length-pageIndex===1){
						let demo1={corrent:pageIndex+2,xing:[]}
						data.push(demo1)
					}
					localStorage.setItem('data',JSON.stringify(data))
					break;
				case Number(score)>=80:
					if(data[pageIndex].xing.length<2){
						data[pageIndex].xing=[1,2];
					}
					if(data.length-pageIndex===1){
						let demo2={corrent:pageIndex+2,xing:[]}
						data.push(demo2)
					}
					localStorage.setItem('data',JSON.stringify(data))
					break;
				case Number(score)>=60:
					if(data[pageIndex].xing.length<1){
						data[pageIndex].xing=[1];
					}
					localStorage.setItem('data',JSON.stringify(data))
					break;
				default:
					break;
			}
		}
	}
	
}

//将state映射到props函数
function mapStateToProps(state){
	return{
		...state
	}
}

//将修改state数据的方法，映射到props，默认会传入store里的dispatch方法
function mapDispatchToProps(dispatch){
	return {
		
		
	}
}	
//将上面的这两个方法，将数据仓库的state和修改state的方法映射到组件上，形成新的组件
const App=connect(
	mapStateToProps,
	mapDispatchToProps
)(Counter)
export default App