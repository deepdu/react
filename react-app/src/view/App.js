import React from "react";
import {connect} from 'react-redux';
import {Button} from 'antd-mobile';
class Counter extends React.Component{
	render(){
		if(window.screen.width<=600){
			return(
				<div className="app">
					<div className="cnt">
						<Button onClick={this.onRanDom} type="primary" className="bnt1">随机答题</Button>
						<Button onClick={this.onOrder} type="primary" className="bnt1">闯关答题</Button>
					</div>
				</div>
			)
		}else{
			return(
				<div>
					<div className="cnt">
						<Button onClick={this.onRanDom} type="primary" className="bnt1">随机答题</Button>
						<Button onClick={this.onOrder} type="primary" className="bnt1">闯关答题</Button>
					</div>
				</div>
			)
		}
		
	}
	onRanDom=()=>{
		this.props.history.push('/suiji')
	}
	onOrder=()=>{
		this.props.history.push('/guanqia')
	}
}

//将state映射到props函数
function mapStateToProps(state){
	return{
		...state
	}
}

//将修改state数据的方法，映射到props，默认会传入store里的dispatch方法
function mapDispatchToProps(dispatch){
	return {
		
		
	}
}	
//将上面的这两个方法，将数据仓库的state和修改state的方法映射到组件上，形成新的组件
const App=connect(
	mapStateToProps,
	mapDispatchToProps
)(Counter)
export default App